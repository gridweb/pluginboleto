<?php

/**
 * Created by PhpStorm.
 * User: vitor
 * Date: 04/05/16
 * Time: 21:56
 */
//

class BoletosController extends BoletosAppController
{

    public $components = array(
        'Boletos.BoletoBb',
        'Boletos.BoletoBradesco',
        'Boletos.BoletoCefSigcb',
        'Boletos.BoletoItau',
        'Boletos.BoletoSantBan',
        'Boletos.ReaderRetComponent'
    );

    public function index()
    {

        $this->autoRender = false;
        $dados = array(
            'sacado' => 'Fulano de Tal',
            'endereco1' => 'Rua do funal de tal, 88',
            'endereco2' => 'Curitiba/PR',
            'valor_cobrado' => 100.56,
            'pedido' => 5 // Usado para gerar o número do documento e o nosso número.
        );

        $this->renderBoleto('BB', $dados);

    }

    public function renderBoleto($banco, $dados)
    {
        $banco = isset($_GET['banco']) ? $_GET['banco'] : '';

        switch ($banco) {
            case 'BB' :
                return $this->BoletoBb->render($dados);
                break;
            case 'BRADESCO' :
                return $this->BoletoBradesco->render($dados);
                break;
            case 'CEF' :
                return $this->BoletoCefSigcb->render($dados);
                break;
            case 'ITAU' :
                return $this->BoletoItau->render($dados);
                break;
            case 'SANT' :
                return $this->BoletoSantBan->render($dados);
                break;
        }

    }
}