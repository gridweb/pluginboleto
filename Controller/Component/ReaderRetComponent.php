<?php

class ReaderRetComponent
{
    protected $data;

    public function initialize(Controller $controller)
    {

    }

    public function startup(Controller $controller)
    {

    }

    public function beforeRender($controller)
    {

    }

    public function shutdown($controller)
    {

    }

    public function getFormat($banco, $type='240'){

        $arr = [
            'BB' => [
                '240' => [
                    'tipo_registro' => [
                        'pos' => [8, 8],
                        'movimento' => 3
                    ],
                    'movimento' => [
                        'id_titulo_banco' => [
                            'pos' => [203, 222]
                        ],
                        'valor_titulo' => [
                            'pos' => [100, 114]
                        ],
                        'valor_pagamento' => [
                            'pos' => [153, 167]
                        ],
                        'id_ocorrencia' => [
                            'pos' => [231, 240]
                        ]
                    ]
                ]
            ],
            'BRADESCO' => [
                '400' => [
                    'tipo_registro' => [
                        'pos' => [1, 1],
                        'movimento' => 1
                    ],
                    'movimento' => [
                        'id_titulo_banco' => [
                            'pos' => [71, 82]
                        ],
                        'valor_titulo' => [
                            'pos' => [127, 139]
                        ],
                        'valor_pagamento' => [
                            'pos' => [127, 139]
                        ],
                        'id_ocorrencia' => [
                            'pos' => [109, 110]
                        ]
                    ]
                ]
            ],
            'CEF' => [
                '400' => [
                    'tipo_registro' => [
                        'pos' => [1, 1],
                        'movimento' => 1
                    ],
                    'movimento' => [
                        'id_titulo_banco' => [
                            'pos' => [117, 126]
                        ],
                        'valor_titulo' => [
                            'pos' => [153, 165]
                        ],
                        'valor_pagamento' => [
                            'pos' => [153, 165]
                        ],
                        'id_ocorrencia' => [
                            'pos' => [109, 110]
                        ]
                    ]
                ]
            ],
            'ITAU' => [
                '400' => [
                    'tipo_registro' => [
                        'pos' => [1, 1],
                        'movimento' => 1
                    ],
                    'movimento' => [
                        'id_titulo_banco' => [
                            'pos' => [127, 134]
                        ],
                        'valor_titulo' => [
                            'pos' => [153, 165]
                        ],
                        'valor_pagamento' => [
                            'pos' => [153, 165]
                        ],
                        'id_ocorrencia' => [
                            'pos' => [109, 110]
                        ]
                    ]
                ]
            ],
            'SANT' => [
                '400' => [
                    'tipo_registro' => [
                        'pos' => [1, 1],
                        'movimento' => 1
                    ],
                    'movimento' => [
                        'id_titulo_banco' => [
                            'pos' => [127, 134]
                        ],
                        'valor_titulo' => [
                            'pos' => [153, 165]
                        ],
                        'valor_pagamento' => [
                            'pos' => [153, 165]
                        ],
                        'id_ocorrencia' => [
                            'pos' => [109, 110]
                        ]
                    ]
                ]
            ]
        ];

        if(!isset($arr[$banco][$type])){
            exit("Configuração {$type} não encontrada para o banco {$banco}!");
        }
        return $arr[$banco][$type];
    }

    private function getByPos($v, $pos){
        $length = $pos[1] - ($pos[0] - 1);
        return substr($v, ($pos[0] - 1), $length);
    }

    public function parse($file, $banco, $type='240')
    {
        $data = file($file) or die("Unable to open file!");

        $this->format = $this->getFormat($banco, $type);

        $array_key = array_keys($this->format['movimento']);
        $primary_key = $array_key[0];

        // parse tipos de registros
        $mov = array();
        foreach ($data as $k => $v) {

            $r = $this->getByPos($v, $this->format['tipo_registro']['pos']);

            if($this->format['tipo_registro']['movimento'] == $r){
                $mov[] =  $v;
            }
        }
        $result = array();
        foreach ($mov as $v){
            $key = (int)$this->getByPos($v, $this->format['movimento'][$primary_key]['pos']);
            $valor = $this->getByPos($v, $this->format['movimento']['valor_titulo']['pos']);
            $valor_pagamento = $this->getByPos($v, $this->format['movimento']['valor_pagamento']['pos']);
            $id_ocorrencia = $this->getByPos($v, $this->format['movimento']['id_ocorrencia']['pos']);

            $result[$key] = [
                'valor' => ($valor*0.01),
                'ocorrencia' => str_pad($id_ocorrencia, 2, '0', STR_PAD_LEFT) ,
                'valor_pagamento' => ($valor_pagamento*0.01)
            ];
        }

        echo '<h1>'.$banco.' CNAB'.$type.'</h1>';

        echo '<pre>';
        print_r($mov);
        print_r($result);
        echo '</pre>';
        exit();

        return $result;
    }
}