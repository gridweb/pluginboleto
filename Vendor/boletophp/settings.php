<?php

$taxa_boleto = isset($dadosboleto['taxa']) ? $dadosboleto['taxa'] : 0;
$demonstrativo1 = isset($dadosboleto['demonstrativo1']) ? $dadosboleto['demonstrativo1'] : '';
$demonstrativo2 = isset($dadosboleto['demonstrativo2']) ? $dadosboleto['demonstrativo2'] : '';
$demonstrativo3 = isset($dadosboleto['demonstrativo3']) ? $dadosboleto['demonstrativo3'] : '';
$instrucoes1 = isset($dadosboleto['instrucoes1']) ? $dadosboleto['instrucoes1'] : '';
$instrucoes2 = isset($dadosboleto['instrucoes2']) ? $dadosboleto['instrucoes2'] : '';
$instrucoes3 = isset($dadosboleto['instrucoes3']) ? $dadosboleto['instrucoes3'] : '';
$instrucoes4 = isset($dadosboleto['instrucoes4']) ? $dadosboleto['instrucoes4'] : '';

$dadosboleto = Set::merge($dadosboleto, Configure::read('Boleto'));

$dadosboleto['demonstrativo1'] = $demonstrativo1;
$dadosboleto['demonstrativo2'] = $demonstrativo2;
$dadosboleto['demonstrativo3'] = $demonstrativo3;
$dadosboleto['instrucoes1'] = $instrucoes1;
$dadosboleto['instrucoes2'] = $instrucoes2;
$dadosboleto['instrucoes3'] = $instrucoes3;
$dadosboleto['instrucoes4'] = $instrucoes4;