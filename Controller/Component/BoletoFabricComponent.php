<?php

class BoletoFabricComponent
{
    protected $data;

    public function initialize(Controller $controller)
    {

    }

    public function startup(Controller $controller)
    {

    }

    public function beforeRender($controller)
    {

    }

    public function shutdown($controller)
    {

    }

    public function get($banco)
    {
        switch ($banco) {
            case 'BB' :
                return new BoletoBbComponent();
                break;
            case 'BRADESCO' :
                return new BoletoBradescoComponent();
                break;
            case 'CEF' :
                return new BoletoCefSigcbComponent();
                break;
            case 'ITAU' :
                return new BoletoItauComponent();
                break;
            case 'SANT' :
                return new BoletoSantBanComponent();
                break;
        }
    }
}