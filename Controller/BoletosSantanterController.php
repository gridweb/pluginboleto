<?php

/**
 * Created by PhpStorm.
 * User: vitor
 * Date: 04/05/16
 * Time: 21:56
 */
//

class BoletosSantanderController extends BoletosAppController
{

//    public $components = array('Boletos.BoletoBb');
//    public $components = array('Boletos.BoletoBradesco');
//    public $components = array('Boletos.BoletoCefSigcb');
//    public $components = array('Boletos.BoletoItau');
    public $components = array('Boletos.BoletoSantBan');

    public function index() {

        $this->autoRender = false;
        $dados = array(
            'sacado' => 'Fulano de Tal',
            'endereco1' => 'Rua do funal de tal, 88',
            'endereco2' => 'Curitiba/PR',
            'valor_cobrado' => 100.56,
            'pedido' => 5 // Usado para gerar o número do documento e o nosso número.
        );
//        $this->BoletoBb->render($dados);
        $this->BoletoSantBan->render($dados);
    }
}